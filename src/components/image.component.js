import React from 'react';
import styled from 'styled-components';

export default function SippyImage(props) {
    const alignment = props.alignment
    const src = props.src;
    const alt = props.alt;
    const width = props.width;
    const ImageContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: ${ alignment !==undefined ? alignment : 'center' };
    & img {
        width: ${width !== undefined ? width : 60}%;
    }
`
    return (
        <ImageContainer>
            <img src={src} alt={alt}/>
        </ImageContainer>
    )
}
