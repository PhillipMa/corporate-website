import React from 'react';
import styled from 'styled-components';

const StyledValue = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: #ec8d2f;
    margin-right: 60px;
    &:last-child {
        margin-right: 0;
    }
    & .sippy-value-number {
        margin-top: 8px;
        font-size: 40px;
        font-weight: 800;
    }
    & .MuiCircularProgress-root {
        color: #ec8d2f;
        width: 90px !important;
        height: 90px !important;
        margin-bottom: 8px;
    }
    & .sippy-value-text {
        color: white;
    }
`

export default function Value(props) {
    const value = props.value;
    const text = props.text;
    return(
        <StyledValue>
            { /* <CircularProgress variant="determinate" value={value} /> */} 
            <span className="sippy-value-number">{value}</span>
            <span className="sippy-value-text">{text}</span>
        </StyledValue>
    ) 
}