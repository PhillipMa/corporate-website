import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import Footer from './footer.component';

// styles

const Container = styled.div`
    width: 100%;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .prices-container {
        width: 100%;
        display: flex;
        flex-direction: row;
        & .price-container {
            margin-right: 32px;
            width: 100%;
            &:last-child {
                margin-right: 0;
            }
            .price-table {
                background: #2f2c2e;
                display: flex;
                flex-direction: column;
                border: 1px solid #FFF;
                & .price-table-header {
                    display: flex;
                    width: 100%;
                    border-bottom: 1px solid #FFF;
                    & .price-table-header-item {
                        width: 100%;
                        color: #FFF;
                        text-align: center;
                        font-weight: 600;
                        padding: 12px;
                        border-right: 1px solid #FFF;
                        &:last-child {
                            border-right: none;
                        }
                    }
                }
                & .price-table-body {
                    width: 100%;
                    display: flex;
                    flex-direction: column;
                    & .price-table-body-row {
                        display: flex;
                        flex-direction: row;
                        width: 100%;
                        cursor: pointer;
                        transition: .2s all ease;
                        &:nth-child(even) {
                            background: #dad9d6;
                        }
                        &:nth-child(odd) {
                            background: #f6f6f6;
                        }
                        &:hover {
                            background: #f8941e;
                        }
                        & .price-table-body-row-item {
                            width: 100%;
                            color: #2f2b2e;
                            text-align: center;
                            font-weight: 500;
                            padding: 12px;
                            transition: .2s all ease;
                        }
                    }
                }
            }
        } 
    } 

`
// helper functions

export default function ComparePrices() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <Title title="Compare prices"/>
                <Paragraph size="medium" text="Value is critical, while our Softswitch is loaded with features and boasts security and stability as its core foundation, it does not cost as much as you would expect. Our company believes in fair value so we price our solutions to address market needs in both products and price.  The result of our fair pricing practices: customers all over the world who constantly recommend our Softswitch to their peers."/>
                <div className="prices-container">
                    <div className="price-container">   
                        <Title title="Shared Hosting"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>Monthly Fee</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>100</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$225.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>15</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>250</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$335.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>30</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>500</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$565.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>45</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>1000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$710.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>60</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$1020.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>75</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>5000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$1475.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>90</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="price-container">
                        <Title title="FLEX Rent with Purchase Option"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>Flex Monthly Fee</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>75</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$550.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>3000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>90</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$665.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>4000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>105</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$785.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>5000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>120</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$895.00</span>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="price-container">
                        <Title title="One Time Purchase"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>Total Payment</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>75</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$7,360.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>3000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>90</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$8,165.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>4000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>105</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$8,970.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>5000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>120</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$9,775.00</span>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
        <Footer/>
        </>
    )
}