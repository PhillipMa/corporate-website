import {useEffect} from 'react';

export default function Home() {
    useEffect(() => {
        // find nav bar
        const navBar = document.getElementsByClassName("sippy-nav")[0];
        navBar.classList.add("home-active");
        return () => {
            navBar.classList.remove("home-active")
        }
      }, []);
    return null
}