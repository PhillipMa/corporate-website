import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import Footer from './footer.component';
import { ReactComponent as Support } from '../assets/svg/support.svg';
import SVG from './svg.component';
const Container = styled.div`
    width: 100%;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .sippy-subtitle {
        font-size: 22px;
        color: #ec8d2f;
        margin-bottom: 32px;
    }
    & .sippy-list {
        width: 100%;
        color: #afafaf;
        margin: 0;
        margin-bottom: 60px;
        font-size: 20px;
        & li {

            margin-bottom: 8px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    strong {
        color:#ec8d2f;
    }
`
const ItemButton = styled.a`
    width: 80px;
}
`

export default function TechnicalInformation() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <Title title="Support and Documentation"/>
                <div className="flex flex-row mb4">
                    <ItemButton className="mr4" target="_blank" href="https://support.sippysoft.com/support/home"><SVG element={Support} width={100} /></ItemButton>
                    
                </div>
                <Paragraph size="medium" text="Whether you purchase or rent a license, Sippy will provide you and your team onboarding, online training and a comprehensive library of online documentation."/>
                <span className="sippy-subtitle">Key Advantages:</span>
                <ul className="sippy-list">
                    <li>
                        <span>Sippy products are 100% developed, controlled and maintained in-house</span>
                    </li>
                    <li>
                        <span>Customisation and Feature development: Sippy can customize or improve its products according to market needs and customer requests, without restriction from any external vendors/service</span>
                    </li>
                    <li>
                        <span>Complete control of development roadmap, patches, security releases, and features</span>
                    </li>
                    <li>
                        <span>Quick support as the team has full access to the code</span>
                    </li>
                    <li>
                        <span>Flexibility: Sippy has flexibility to make changes to meet local business/legal/gov requirement</span>
                    </li>
                    <li>
                        <span>Reliable: Sippy uses the latest version of the FreeBSD operating system which is far more robust, scalable and secure system than Linux</span>
                    </li>
                    <li>
                        <span>High-quality support: 24/7 support by ‘real’ people. Every representative is trained in-house by Sippy.  Sippy Support provides on-boarding, training and general support; as well as customized support services and consultations. Competitors offer auto response and incompatible support.</span>
                    </li>
                </ul>
                <Paragraph size="medium" text="All internal documentation is updated on a regular basis ensuring that you have access to vital information you and your team can use to know your Softswitch inside and out."/>
                <Title title="Security"/>
                <Paragraph size="medium" text="Industry-wide, Sippy’s Softswitch is known for being secure.  The technical team monitors security parameters every hour of every day to make sure that we are on top of any security issues that might come up.  Beyond our 24/7 monitoring, our Softswitch sits on the secure FreeBSD platform which makes its performance particularly robust, and secure.  The security features built-in to our Softswitch guarantee that your business relies on secure technology."/>
                <span className="sippy-subtitle">Password security controls:</span>
                <ul className="sippy-list">
                    <li>
                        
                        <span>7+ digit/number/symbol length password prompting</span>
                    </li>
                    <li>
                        
                        <span>Manageable password policies</span>
                    </li>
                    <li>
                       
                        <span>Password Strength Indicator</span>
                    </li>
                    <li>
                        
                        <span>Multifactor Authentication (including Google Authenticator)</span>
                    </li>
                </ul>
                <span className="sippy-subtitle">Sippy’s key security features:</span>
                <ul className="sippy-list">
                    <li>
                        <strong>Multi Factored Authentication </strong>
                        <span>Switch operators can enable multi-factored authentication for web users.  Users have the option to use an email based system or google authentication</span>
                    </li>
                    <li>
                        <strong>SSL Certificates </strong>
                        <span>SSL certificates can be generated from the softswitch via Let's Encrypt.</span>
                    </li>
                    <li>
                        <strong>Encrypted passwords </strong>
                        <span>Passwords are encrypted when saved and cannot be retrieved from the database</span>
                    </li>
                    <li>
                        <strong>Separate VoIP credentials for Digest Authentication </strong>
                        <span>Different web and SIP traffic credentials Accounts for accessibility and user security</span>
                    </li>
                    <li>
                        <strong>Sophisticated VoIP traffic Authentication </strong>
                        <span>Combined Remote IP, Incoming CLI and Incoming CLD authentication to authenticate traffic to an Account</span>
                    </li>
                    <li>
                        <strong>Account session limits </strong>
                        <span>Total session limits (inbound/outbound combined) protect an Account from traffic flooding resulting from a hacked Account</span>
                    </li>
                    <li>
                        <strong>Account CPS limits </strong>
                        <span>CPS limits protect an Account from traffic flooding resulting from a hacked Account</span>
                    </li>
                    <li>
                        <strong>Built-in Firewall for additional network security layer </strong>
                        <span>Web, SSH, DB, and SIP traffic firewall rules</span>
                    </li>
                    <li>
                        <strong>Web Access Control restriction </strong>
                        <span>Restrict staff web login to a specific IP addresses/location</span>
                    </li>
                    <li>
                        <strong>User Audit logs </strong>
                        <span>Monitor changes made by staff and track when they access the softswitch and from where</span>
                    </li>
                    <li>
                        <strong>Connection channel and CPS limits </strong>
                        <span>restrictive routing and capping of calls to specified upstream locations</span>
                    </li>
                    <li>
                        <strong>In-house framework </strong>
                        <span>(Sippy B2BUA & RTPproxy) ensures Sippy is not remotely vulnerable to industry-wide security and fraud attacks</span>
                    </li>
                    <li>
                        <strong>Mask network topology </strong>
                        <span>using the media relay features to proxy RTP traffic and hide IPs</span>
                    </li>
                </ul>
            </div>
        </Container>
        <Footer/>
        </>
    )
}
