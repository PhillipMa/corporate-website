import React from 'react';
import Video from './video.component';

export default function IntroSection(props) {
    const type = props.type;
    const video = props.video;
    const hasButton = props.hasButton;
    const buttonLabel = props.buttonLabel;
    return (
        <div>
            {
                type === "icon"
                ?
                    null
                :
                <Video src={video} hasButton={hasButton} buttonLabel={buttonLabel} />
            }
        </div>
    )
}