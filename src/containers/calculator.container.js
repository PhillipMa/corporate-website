import React, { useState } from 'react';
import styled from 'styled-components';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

// styles
const CalculatorContainer = styled.div`
    border: 2px solid white;
    border-radius: 4px;
    transition: .2s all ease;
`
const SelectControl = styled(Select)`
    color: white !important;
    & svg {
        color: white;
    }
    &.MuiInput-underline:before {
        border-color: white !important;
    }
`


export default function Calculator() {
    // state
    const [ hostingType, setHostingType ] = useState("");
    const [ hostingOption, setHostingOption ] = useState(0);
    const [ setUpFee, setSetUpFee ] = useState(0);
    
    // handle state
    const HandlingHostingType = (e) => {
        const value = e.target.value;
        let fee = 0;
        if (value === 'shared-hosted') {
            fee = 45;
        }
        else if (value === 'dedicated-premium' || value === 'self-hosted purchase' || value === 'dedicated-standard') {
            fee = 200;
        }
        else {
            fee = 0;
        }
        setHostingOption(0);
        setSetUpFee(fee);
        setHostingType(value);
    }
    const HandleHostingOption = (e) => {
        setHostingOption(e.target.value);
    }
    return(
        <CalculatorContainer className="pa4 w-100 w-40-ns justify-center flex flex-column">
            <div className="flex flex-column flex-row-ns mb4">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Hosting:</h1>
                <FormControl className="w-100">
                    <SelectControl
                        labelId="hosting-type-select-label"
                        id="hosting-type-select"
                        value={hostingType}
                        onChange={HandlingHostingType}
                    >
                        <MenuItem value="shared-hosted">Shared Hosted</MenuItem>
                        <MenuItem value="dedicated-standard">Dedicated (standard)</MenuItem>
                        <MenuItem value="dedicated-premium">Dedicated (premium)</MenuItem>
                        <MenuItem value="self-hosted purchase">Self-Hosted Purchase</MenuItem>
                    </SelectControl>
                </FormControl>
            </div>
            <div className="flex flex-column flex-row-ns mb4">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Capacity:</h1>
                <FormControl className="w-100">
                    {
                        hostingType === "shared-hosted"
                        ?
                            <SelectControl
                                labelId="call-capacity-select-label"
                                id="call-capacity-select"
                                value={hostingOption}
                                onChange={HandleHostingOption}
                            >
                                <MenuItem value={225} >100 CC / 15 CPS</MenuItem>
                                <MenuItem value={335} >250 CC / 30 CPS</MenuItem>
                                <MenuItem value={565} >500 CC / 45 CPS</MenuItem>
                                <MenuItem value={710} >1000 CC / 60 CPS</MenuItem>
                                <MenuItem value={1020} >2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={1475} >5000 CC / 90 CPS</MenuItem>
                                </SelectControl>
                        :
                        hostingType === "dedicated-standard"
                        ?
                            <SelectControl
                            labelId="call-capacity-select-label"
                            id="call-capacity-select"
                            value={hostingOption}
                            onChange={HandleHostingOption}
                            >
                                <MenuItem value={1295} >2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={1460} >3000 CC / 90 CPS</MenuItem>
                                <MenuItem value={1650} >4000 CC / 105 CPS</MenuItem>
                                <MenuItem value={1860} >5000 CC / 120 CPS</MenuItem>
                                </SelectControl>
                        :
                        hostingType === "dedicated-premium" || hostingType === "self-hosted purchase"
                        ?
                            <SelectControl
                                labelId="call-capacity-select-label"
                                id="call-capacity-select"
                                value={hostingOption}
                                onChange={HandleHostingOption}
                            >
                                <MenuItem value={ hostingType === "dedicated-premium" ? (2266) : 7360} >2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (2555) : 8165} >3000 CC / 90 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (2888) : 8970} >4000 CC / 105 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (3255) : 9775} >5000 CC / 120 CPS</MenuItem>
                                </SelectControl>
                        : null
                    }
                </FormControl>
            </div>
            <div className="flex flex-column flex-row-ns">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Total:</h1>
                <span className="w-100 white tr f3 fw7">$ {(hostingOption).toFixed(2)}</span>
            </div>
            <div class="w-100 white tc mt4">
            <p className="f6 mb2">
                Account install fee of ${setUpFee}.00 applies.
            </p>
            <p className="f6">
                {
                    hostingType === 'purchase self-hosted'
                    ?
                    "Price includes first year standard support. Install fee is charged only on first invoice."
                    :
                    "All prices are monthly in USD. Install fee is charged only on first invoice."
                }
                </p>
            </div>
        </CalculatorContainer>
    )
}